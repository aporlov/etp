﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Ionic.Zip;
using NLog;
namespace GZFtp
{
    public class FtpWork
    {
        public string FTPServer { get; set; }
        public FtpWebRequest regFTP { get; set; }
        public static Logger log;
        public FtpWork(string _FTPServer)
        {
            FTPServer = _FTPServer;
            log = LogManager.GetLogger("ETPLog");
        }
        /// <summary>
        /// Функция для получения списка файлов с ФТП сайта госзакупок
        /// </summary>
        /// <returns> список файлов по фильтру Year "notification inc"</returns>
        public List<string> GetFileList()
        {
            List<string> buffer = new List<string>();
            try
            {
                regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(FTPServer));
                regFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)regFTP.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                while (!reader.EndOfStream)
                {
                    buffer.Add(reader.ReadLine());
                }
                reader.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                log.Error("FTPWorks.GetFileList:Не могу закачать список файлов {0} {1}", FTPServer, ex.Message);
            }
           // buffer.ForEach(c=>log.Error("FTPWorks.GetFileList: Список файлов на фтп {0}",c)); 
            return buffer;
        }
        /// <summary>
        /// Закачать указанный файл и распаковать его и вернуть список xml
        /// </summary>
        /// <param name="NameFile"> имя файла</param>
        /// <returns>Из распакованных файлов извлечь XML структуру</returns>
        public List<XElement> DownloadFile(string NameFile)
        {
            List<XElement> xmlResult = new List<XElement>();
            try
            {
                string temppath = GetTempPath();
                FileStream outputStream = new FileStream(temppath + NameFile, FileMode.Create);
                regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(Path.Combine(FTPServer,NameFile))); //"ftp://free:free@" + FTPServer + ":21/fcs_regions/" + Region + "/notifications/currMonth/" + NameFile
                regFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                regFTP.UseBinary = true;
                FtpWebResponse response = (FtpWebResponse)regFTP.GetResponse();
                Stream responseStream = response.GetResponseStream();
                int bufferSize = 2048;
                int readCount;
                byte[] buffer = new byte[bufferSize];

                readCount = responseStream.Read(buffer, 0, bufferSize);
                while (readCount > 0)
                {
                    outputStream.Write(buffer, 0, readCount);
                    readCount = responseStream.Read(buffer, 0, bufferSize);
                }
                responseStream.Close();
                outputStream.Close();
                response.Close();
                log.Trace("FTPWork.DownloadFile: закачен файл {0}", NameFile);
                try
                {
                    using (ZipFile zip = ZipFile.Read(temppath + NameFile))
                    {
                        //zip.ExtractAll(temppath,ExtractExistingFileAction.OverwriteSilently);  // true => overwrite existing files   } } 
                        foreach (ZipEntry e in zip)
                        {
                            MemoryStream memorystream = new MemoryStream();
                            e.Extract(memorystream);
                            byte[] b = memorystream.ToArray();
                            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                            var text = encoding.GetString(b, 0, b.Length);
                            xmlResult.Add(XElement.Parse(text, LoadOptions.PreserveWhitespace));
                            memorystream.Close();
                        }
                    }
                    File.Delete(temppath + NameFile);

                }
                catch (Exception ex)
                {
                    log.Error("FTPWork.DownloadFile: Не могу распаковать файл: {0} {1}", NameFile, ex.Message);  
                }
            }
            catch (Exception ex)
            {
                log.Error("FTPWork.DownloadFile: Не могу скачать файл: {0} {1}", NameFile, ex.Message); 
            }
            return xmlResult;
        }
        private static void CopyStream(Stream source, Stream target)
        {
            const int bufSize = 0x1000;
            byte[] buf = new byte[bufSize];
            int bytesRead = 0;
            while ((bytesRead = source.Read(buf, 0, bufSize)) > 0)
                target.Write(buf, 0, bytesRead);
        }
        public string GetTempPath()
        {
            string path =
               System.Environment.GetEnvironmentVariable("TEMP");
            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }
            return path;
        }
    }
}
