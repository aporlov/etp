﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mailer
{
    class WorkingDatabase
    {
        
        private DataClasses1DataContext db;
        public WorkingDatabase(string _connectionstring)
        {
            db = new DataClasses1DataContext(_connectionstring);
         }
        
        public void Save()
        {
            db.SubmitChanges();
        }
        /// <summary>
        /// Получить список правил для конкретной торговой системы
        /// </summary>
        /// <param name="NaimTradesystem"></param>
        /// <returns></returns>
       
        public IQueryable<Letters> FindAllLetters()
        {
            return from n in db.Letters
                   where n.SendStatus == false
                   select n;
         }
        public void UpdateLetterStatus(Int64 LetterId)
        {
            var _letter = db.Letters.SingleOrDefault(n => n.LetterId == LetterId);
            _letter.SendStatus = true;
            _letter.SendDate = DateTime.Now;
        }

    }
    
}
