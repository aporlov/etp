﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using NLog;

namespace Mailer
{
    class SendMail
    {
        protected string smtpserver;
        protected WorkingDatabase db;
        public static Logger log;
        public SendMail(string connectionstring, string _smtpserver, bool loglevel)
        {
            this.smtpserver = _smtpserver;
            log = LogManager.GetLogger("ETPLog");
            db = new WorkingDatabase(connectionstring);
        }
        public void SendALL()
        {
            List<Letters> _list = ListLettersFromBase();
            if (_list!=null)
            {
                foreach (Letters n in _list)
                {
                      
                    if (SendTo(n))
                    {
                        db.UpdateLetterStatus(n.LetterId);
                        db.Save();
                    };
                }
            }
                
        }
        public List<Letters> ListLettersFromBase()
        {
            List<Letters> _list = null;
            try
            {
                _list = db.FindAllLetters().ToList();
                return _list;
            }
            catch (Exception e)
            {
               log.Error("SendMail.ListLettersFromBase: {0} "  + e.Message.ToString());
                return null;
            }
            
        }
        public bool SendTo(Letters _letter)
        {
              try

            {                    
              // create the email message 
                MailMessage message = new MailMessage(

                   _letter.SendFrom,

                   _letter.SendTo,

                   _letter.SendSubject,

                   _letter.SendMessage);
                message.IsBodyHtml = true;
                 // create smtp client at mail server location
                SmtpClient client = new  SmtpClient(this.smtpserver);
                #region
                /*
                  System.IO.MemoryStream ms = new System.IO.MemoryStream();
                System.IO.StreamWriter writer = new System.IO.StreamWriter(ms,Encoding.GetEncoding("windows-1251"));       
                string[] buffer=Regex.Split(_letter.SendMessage, "</p><p>");
                List<string> list = new List<string>();
                list.Add("Код аукциона;Название аукциона;Место поставки;Организация;Дата окончания приема заявок;Сумма контракта;Ссылка на аукцион;");
                foreach (string item in buffer)
                {
                    int CodeBegin = item.IndexOf("<b>Код аукциона:</b>")+20;
                    int CodeEnd = item.IndexOf("<br/><b>Название аукциона:</b>");
                    int NameBegin = CodeEnd+30;
                    int NameEnd = item.IndexOf("<br/><b>Место поставки:</b>");
                    int PlaceBegin = NameEnd+27;
                    int PlaceEnd = item.IndexOf("<br/><b>Организация:</b>");
                    int OrgBegin = PlaceEnd+24;
                    int OrgEnd = item.IndexOf("<br/><b>Дата окончания приема заявок:</b>");
                    int DataBegin = OrgEnd+41;
                    int DataEnd = item.IndexOf("<br/><b>Начальная сумма контракта:</b>");
                    int SumBegin = DataEnd + 38; 
                    int SumEnd = item.IndexOf("<br/><a href=");
                    int LinkBegin = DataEnd+13;
                    int LinkEnd = item.IndexOf(">Посмотреть на сайте")-1;
                    string Code= "'"+item.Substring(CodeBegin,CodeEnd-CodeBegin)+"'";
                    string Name= item.Substring(NameBegin,NameEnd-NameBegin).Replace("\n","");
                    string Place= item.Substring(PlaceBegin,PlaceEnd-PlaceBegin).Replace("\n","");
                    string Org = item.Substring(OrgBegin,OrgEnd-OrgBegin).Replace("\n","");
                    string Data = item.Substring(DataBegin,DataEnd - DataBegin);
                    string Sum = item.Substring(SumBegin, SumEnd - SumBegin);
                    string Link = item.Substring(LinkBegin,LinkEnd-LinkBegin);
                    StringBuilder sb = new StringBuilder();
                    sb.Append(Code).Append(';').Append(Name).Append(';').Append(Place).Append(';').Append(Org).Append(';').Append(Data).Append(';').Append(Link).Append(';');
                    list.Add(sb.ToString());
                }
                foreach (var item in list)
                {
                    writer.WriteLine(item);
                }
                writer.Flush();
                ms.Position = 0; 
                System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Text.Plain);
                System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(ms, ct);
                attach.ContentDisposition.FileName = "List.csv";
                message.Attachments.Add(attach);
*/
                #endregion
                client.Send(message);
                client.Dispose();
                log.Info("OK: SendMail.cs SendTo Письмо отправлено адресату LetterID={0}  " ,_letter.LetterId);
                return true;
            }
            catch (Exception ex)
            {
                log.Error("SendMail.SendTo: Невозможно отправить письмо LetterID = {0} , {1}",_letter.LetterId , ex.Message);
                return false;
            }

        }
     }
    
}
