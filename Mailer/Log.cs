﻿using System;
using System.IO;
using System.Text;

namespace Mailer
{
    /// <summary>
    /// Создание лог файла Level 0 - none 1- verbose 2 -debug
    /// </summary>
    class Log
    {
        public bool Level;

        public Log()
            : this(false)
        {
        }
        public Log(bool _level)
        {
            this.Level = _level;
        }

        public string GetTempPath()
        {
            string path =
               System.Environment.GetEnvironmentVariable("TEMP");
            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }
            return path;
        }

        public void LogMessageToFile(string message)
        {
            Encoding encoding1  = Encoding.GetEncoding(1251);

            System.IO.StreamWriter sw = new  StreamWriter(GetTempPath() + "LogTradeSystem.txt",true,encoding1); // Change filename
            try
            {
                string logLine =
                   System.String.Format(
                      "{0:G}: {1}.", System.DateTime.Now, message);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
