﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Mailer
{
    class Program
    {
        
        static void Main(string[] args)
        {
            SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder();
            if (args.Length != 1)
            {
                System.Console.WriteLine("Должен быть один параметр SMTP server");
            }
            else
            {
               
                conn.DataSource = @".\SQLEXPRESS";
                conn.InitialCatalog = "tradesystem";
                conn.IntegratedSecurity = true;
                SendMail _sendmail = new SendMail(conn.ConnectionString, args[0], true);
                _sendmail.SendALL();
            }
          }
    }
}
