﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Analyzing
{
    public class purch
    {
        public String purchid;
        public String purchcode;
        public String purchname;
        public String purchamount;
        public String purchdescription;
        public String publicdatestr;
        public String requestdatestr;
        public String auctionbegindatestr;
        public String purchstatename;
        public String orgname;
        public String requestcount;
        public String purchischanged;
        public String purchurl;
        // код номенклатуры 
        public String code;
        public String region;
    }
}
