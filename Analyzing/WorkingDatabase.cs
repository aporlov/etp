﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Analyzing
{
    public class WorkingDatabase
    {
        private  DataClasses1DataContext db;
        public WorkingDatabase(string _connectionstring)
        {
            try
            {
                db = new DataClasses1DataContext(_connectionstring);
            }
            catch(Exception e)
            {
                System.Console.WriteLine(e);
            }
        }
                   
        public void Save()
        {
            db.SubmitChanges();
        }
        /// <summary>
        /// Получить список правил для конкретной торговой системы
        /// </summary>
        /// <param name="NaimTradesystem"></param>
        /// <returns></returns>
        public IQueryable<Rule> FindAllRules()
        {
            return from n in db.Rules
                   select n;
          }
        public string GetEmail(Guid UserId)
        {
           aspnet_Membership user = db.aspnet_Memberships.SingleOrDefault(n=>n.UserId==UserId);
           return user.Email;
        }
        public string GetPerfomedPurchID(int TradeId)
        {
            var _tradesystem = db.Tradesystems.SingleOrDefault(n => n.TradeId == TradeId);
            return _tradesystem.PerformedPurchId;
        }
        public string GetLastPurchID(int TradeId)
        {
            var _tradesystem = db.Tradesystems.SingleOrDefault(n => n.TradeId == TradeId);
            return _tradesystem.LastPurchId;
        }
        public Region GetRegion(string RegionName, int TradeId)
        {
            var _region = db.Regions.SingleOrDefault(n => n.RegionName == RegionName && n.TradeID == TradeId);
            return _region;
        }
        public void UpdateLastDateToRegion(string RegionName,string LastDate,int TradeId)
        {
            var _region = db.Regions.SingleOrDefault(n => n.RegionName == RegionName && n.TradeID == TradeId);
             _region.LastDate = LastDate;
        }
        public List<string> regionlist(int FZ)
        {
            var _region = db.Regions.Where(c=>c.FZ==FZ).Select(n => n.RegionName).ToList();
            return _region;
        }
        public void UpdatePerfomedPurchID(int TradeId, string LastId)
        {
            var _tradesystem = db.Tradesystems.SingleOrDefault(n => n.TradeId == TradeId);
            _tradesystem.PerformedPurchId = LastId;
        }
        public void UpdateLastPurchID(int TradeId, string LastId)
        {
            var _tradesystem = db.Tradesystems.SingleOrDefault(n => n.TradeId == TradeId);
            _tradesystem.LastPurchId = LastId;
         }
        public void Add<T>(T row)
        {
            db.GetTable(typeof(T)).InsertOnSubmit((T) row);
            
         }
        public IQueryable<Letter> FindAllLetters()
        {
            
            return from n in db.Letters
                   where n.SendStatus == false
                   select n;
        }
        public Tradesystem GetTradeSystem(string NaimTradesystem)
        {
            return db.Tradesystems.SingleOrDefault(n => n.Tradename.Contains(NaimTradesystem));
        }
       
    }
}
