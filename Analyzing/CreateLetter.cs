﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml.Linq;

namespace Analyzing
{
    /// <summary>
    /// Класс для создания писем для пользователей
    /// </summary>
    public class Createletter
    {
        private WorkingDatabase db;
        public string sendFrom { get; set; }
        public string sendSubject { get; set; }
        public string sendTo { get; set; }
        public string sendMessage { get; set; }
        public string NameOfTradesystem { get; set; }
      //  public _Tradesystem tradesystem { get; set; }
        public static Logger log;

        public Createletter(string connectionstring, string _sendFrom, string _NameOfTradesystem)
        {
            this.sendFrom = _sendFrom;
            this.NameOfTradesystem = _NameOfTradesystem;
            this.db = new WorkingDatabase(connectionstring);
            log = LogManager.GetLogger("ETPLog");
        }
        /// <summary>
        /// Основная процедура для обработки 
        /// </summary>
        /// <param name="_purch">Список аукционов</param>
        public void Processing(List<purch> _purch)
        {
            int take = 10;
            List<Rule> rulelist = null;
            //Получить все правила относящиеся к торговой системе
            try
            {
                rulelist = db.FindAllRules().ToList();
            }
            catch (Exception ex)
            {
               log.Error("Createletter.Processing:{0}", ex.Message); 
            }

            //цикл по найденным правилам
            if (rulelist != null)
            {
                var expressionfiltr = PredicateBuilder.True<purch>();
                var expressionfiltrOr = PredicateBuilder.False<purch>();
                foreach (Rule n in rulelist)
                {
                    try
                    {
                        XElement _rulexml = XElement.Parse(n.Rulexml);
                        //поиск аукциона удовлетворяющего правилу         
                        List<purch> find = _purch
                            .Where<purch>(expressionfiltr.And<purch>(expressionfiltrOr.Or<purch>(Filtrpurchname(_rulexml)).Or<purch>(Filtrcode(_rulexml))).And<purch>(Filtrregion(_rulexml)).Compile())
                            .Select(k => k).ToList();
                        //цикл по найденным аукционам и создание электронного письма
                        //новая версия добавлено 14.09.2012
                        int takeover = find.Count() / take;
                        for (int i = 0; i <= takeover; i++)
                        {
                            Letter _letter = new Letter
                            {
                                SendMessage = "",
                                SendTo = db.GetEmail(n.UserId),
                                SendSubject = string.Format("На ЭТП {0} новый аукцион {1}({2})", NameOfTradesystem, DateTime.Now.ToShortDateString(), i + 1),
                                SendFrom = this.sendFrom,
                                SendStatus = false,
                                UserId = n.UserId,
                                SendDate = DateTime.Now
                            };
                            foreach (purch n2 in find.Skip(i * take).Take(take).ToList())
                            {
                                _letter.SendMessage = ReturnSendMessage(_letter.SendMessage, n2);
                            }
                            try
                            {
                                if (_letter.SendMessage.Contains("p"))
                                {
                                    db.Add<Letter>(_letter);
                                    db.Save();
                                    log.Info("OK: Createletter.cs Processing() Письмо создано.Body:{0} SendTO: {1}", _letter.SendTo, n.UserId);
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("Createletter.cs Processing():{0} ", ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    { 
                        log.Error("Createletter.cs Processing():{0} ", ex.Message); 
                    }

                }
            }
        }
        /// <summary>
        /// Создание дерево выражения для purchname
        /// </summary>
        /// <param name="filtr">фильтр юзера в формате xml</param>
        /// <returns>возвращает дерево выражения</returns>
        protected Expression<Func<purch, bool>> Filtrpurchname(XElement filtr)
        {
            char[] charSeparators = new char[] { ' ' };
            string purchname = (string)filtr.Element("purchname");
            if (purchname == null)
            {
                var expressionfiltr = PredicateBuilder.False<purch>();
                return expressionfiltr;
            }
            else
            {
                string[] buffer = purchname.TrimStart().TrimEnd().ToUpper().Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                var expressionfiltr = PredicateBuilder.False<purch>();
                foreach (string n in buffer)
                {
                    string temp = n;
                    expressionfiltr = expressionfiltr.Or(k => k.purchname.ToUpper().Contains(temp));

                }
                return expressionfiltr;
            }

        }

        /// <summary>
        /// Создание дерево выражения для purchdescription
        /// </summary>
        /// <param name="filtr">фильтр юзера в формате xml</param>
        /// <returns>возвращает дерево выражения</returns>
        protected Expression<Func<purch, bool>> Filtrpurchdescription(XElement filtr)
        {
            char[] charSeparators = new char[] { ' ' };
            string purchdescription = (string)filtr.Element("purchdescription");
            if (purchdescription == null)
            {
                var expressionfiltr = PredicateBuilder.True<purch>();
                return expressionfiltr;
            }
            else
            {
                string[] buffer = purchdescription.TrimStart().TrimEnd().ToUpper().Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                var expressionfiltr = PredicateBuilder.False<purch>();
                foreach (string n in buffer)
                {
                    string temp = n;
                    expressionfiltr = expressionfiltr.Or(k => k.purchdescription.ToUpper().Contains(temp));
                }
                return expressionfiltr;
            }


        }
        /// <summary>
        /// Создание дерево выражения для кода номенклатуры
        /// </summary>
        /// <param name="filtr">фильтр юзера в формате xml</param>
        /// <returns>возвращает дерево выражения</returns>
        protected Expression<Func<purch, bool>> Filtrcode(XElement filtr)
        {
            char[] charSeparators = new char[] { ' ' };
            string code = (string)filtr.Element("code");
            if (code == null)
            {
                var expressionfiltr = PredicateBuilder.False<purch>();
                return expressionfiltr;
            }
            else
            {
                string[] buffer = code.TrimStart().TrimEnd().ToUpper().Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                var expressionfiltr = PredicateBuilder.False<purch>();
                foreach (string n in buffer)
                {
                    string temp = n;
                    expressionfiltr = expressionfiltr.Or(k => k.code.ToUpper().IndexOf(temp) == 0);
                }
                return expressionfiltr;
            }
        }

        /// <summary>
        /// Создание дерево выражения для region
        /// </summary>
        /// <param name="filtr">фильтр юзера в формате xml</param>
        /// <returns>возвращает дерево выражения</returns>
        protected Expression<Func<purch, bool>> Filtrregion(XElement filtr)
        {
            char[] charSeparators = new char[] { ' ' };
            string region = (string)filtr.Element("region");
            if (region == null)
            {
                var expressionfiltr = PredicateBuilder.True<purch>();
                return expressionfiltr;
            }
            else
            {
                string[] buffer = region.TrimStart().TrimEnd().ToUpper().Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                var expressionfiltr = PredicateBuilder.False<purch>();
                foreach (string n in buffer)
                {
                    string temp = n;
                    expressionfiltr = expressionfiltr.Or(k => k.region.ToUpper().Contains(temp));
                }
                return expressionfiltr;
            }


        }
        protected string ReturnSendMessage(string sendmessage, purch purch)
        {
            string HTMLBody = "";
            string HTMLTemplatePath = "HTMLTemplate.htm";
            try
            {
                HTMLBody = File.ReadAllText(HTMLTemplatePath);
            }
            catch (Exception ex)
            {
                log.Error("Createletter.cs ReturnSendMessage:{0} ", ex.Message); 
            }

            HTMLBody = HTMLBody.Replace("[Naim]", purch.purchname);
            HTMLBody = HTMLBody.Replace("[Address]", purch.purchdescription);
            HTMLBody = HTMLBody.Replace("[Org]", purch.orgname);
            HTMLBody = HTMLBody.Replace("[EndDate]", purch.requestdatestr);
            HTMLBody = HTMLBody.Replace("[Id]", purch.purchid);
            HTMLBody = HTMLBody.Replace("[Code]", purch.purchcode);
            HTMLBody = HTMLBody.Replace("[Sum]", purch.purchamount);
            HTMLBody = HTMLBody.Replace("[URL]", purch.purchurl);

            //"Название аукциона:<br/>  {0}<br/> Место поставки:<br/>  {1}<br/>Организация:<br/>  {2}<br/>Дата окончания приема заявок:<br/>  {3}<br/>ЭТП: {4} <br/> Подробности по адресу:<br/>  {5}{6}", 

            return sendmessage + HTMLBody;
        }
    }
}
