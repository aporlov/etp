﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Ionic.Zip;
using System.Xml.Linq;
using System.Xml;
using GZFtp;
using NLog;
using Analyzing;

namespace ZakupkiGovFTP
{
    public class ZakupkiGovFTP44 : _Tradesystem
    {
        public ZakupkiGovFTP44(string connectionstring )
            : base(connectionstring, "Cайт госзакупок 44ФЗ") 
        {
            log = LogManager.GetLogger("ETPLog");
            this.FilterForFilesonFTP = (a) => a.Contains("notification");
        }
        /// <summary>
        /// Обработка полученных файлов в xml формате 
        /// </summary>
        /// <param name="goszakupka_root"></param>
        /// <returns></returns>
        public override List<purch> XElementToPurch(XElement goszakupka_root, string _region)
        {
            List<purch> buffer = new List<purch>();
            try
            {
                   
                    XNamespace gml1 = "http://zakupki.gov.ru/oos/export/1";
                    XNamespace gml = "http://zakupki.gov.ru/oos/types/1";
                    XElement goszakupka = goszakupka_root.Element(gml1 + "fcsNotificationEF");
                    if (goszakupka == null) goszakupka = goszakupka_root.Element(gml1 + "fcsNotificationZK"); 
                    if (goszakupka == null) return null;
                    purch purch = new purch();
                    string requestdatestr = goszakupka.Descendants(gml + "endDate").FirstOrDefault().Value;
                    string[] proba = requestdatestr.Substring(0, 10).Split('-');
                    purch.requestdatestr = proba[2] + "." + proba[1] + "." + proba[0];
                    if (DateTime.Parse(purch.requestdatestr) <= DateTime.Now.Date) return null;
                    purch.purchid = goszakupka.Descendants(gml + "id").FirstOrDefault().Value;
                    purch.purchcode = goszakupka.Descendants(gml + "purchaseNumber").FirstOrDefault().Value;
                    purch.purchname = goszakupka.Descendants(gml + "purchaseObjectInfo").FirstOrDefault().Value;
                    purch.purchurl = goszakupka.Descendants(gml + "href").FirstOrDefault().Value;
                    purch.purchdescription = goszakupka.Descendants(gml + "factAddress").FirstOrDefault().Value;
                    purch.orgname = goszakupka.Descendants(gml + "fullName").FirstOrDefault().Value;
                    purch.purchamount = goszakupka.Descendants(gml + "maxPrice").FirstOrDefault().Value;  
                    var purchaseObjects = goszakupka.Descendants(gml + "purchaseObject").ToList();
                    
                      foreach ( var item in purchaseObjects)
                      {
                        purch.code = item.Descendants(gml + "code").FirstOrDefault().Value;
                        if (purch.code == null || purch.code == "")
                        {
                            purch.code = "0000000";
                        }
                        purch bufferpurch = new purch() { auctionbegindatestr = purch.auctionbegindatestr,
                                                          code = purch.code, 
                                                          orgname = purch.orgname, 
                                                          publicdatestr = purch.publicdatestr, 
                                                          purchamount = purch.purchamount, 
                                                          purchcode = purch.purchcode, 
                                                          purchdescription = purch.purchdescription, 
                                                          purchid = purch.purchid, 
                                                          purchischanged = purch.purchischanged, 
                                                          purchname = purch.purchname, 
                                                          purchstatename = purch.purchstatename, 
                                                          purchurl = purch.purchurl, 
                                                          requestcount = purch.requestcount, 
                                                          requestdatestr = purch.requestdatestr,
                                                          region = _region};
                        if (buffer.Where(c => c.purchid == bufferpurch.purchid && c.code.Substring(0, 4) == bufferpurch.code.Substring(0, 4)).ToList().Count == 0)
                            buffer.Add(bufferpurch);
                      }                 
            return buffer.ToList();
            }
            catch (Exception ex)
            {
               log.Error("ZakupkiGovFTP.XElementToPurch:{0}",ex.Message);
            }
            return null; ;     
        }
        public override long GetLastDate(string filename)
        {
            Int64 rt = 0;
            try
            {
                log.Trace("ZakupkiGovFTP44.GetLastDate: Получение даты файла {0}", filename);
                rt = Int64.Parse((filename.Substring(filename.Length - 22, 14).Replace('_', '0')));
                if (rt> 20260531000000) { rt = 0; }
            }
            catch (Exception ex)
            {
                log.Error("ZakupkiGovFTP44.GetLastDate:{0}", ex.Message);
            }
            return rt;
        }
    }
    public class ZakupkiGovFTP223 : _Tradesystem
    {
        public ZakupkiGovFTP223(string connectionstring, string NameOfTradesystem)
            : base(connectionstring, NameOfTradesystem)
        {
            log = LogManager.GetLogger("ETPLog");
            this.FilterForFilesonFTP = (a) => a.Contains("purchaseNotice");
        }
        /// <summary>
        /// Обработка полученных файлов в xml формате 
        /// </summary>
        /// <param name="goszakupka_root"></param>
        /// <returns></returns>
        public override List<purch> XElementToPurch(XElement goszakupka_root, string _region)
        {
            List<purch> buffer = new List<purch>();
            try
            {
                XNamespace gml = "http://zakupki.gov.ru/223fz/purchase/1";
                XNamespace gml1 = "http://zakupki.gov.ru/223fz/types/1";
                purch purch = new purch();
                string requestdatestr = goszakupka_root.Descendants(gml + "submissionCloseDateTime").FirstOrDefault().Value;
                string[] proba = requestdatestr.Substring(0, 10).Split('-');
                purch.requestdatestr = proba[2] + "." + proba[1] + "." + proba[0];
                if (DateTime.Parse(purch.requestdatestr) <= DateTime.Now.Date) return null;
                purch.purchid = goszakupka_root.Descendants(gml + "guid").FirstOrDefault().Value;
                purch.purchcode = goszakupka_root.Descendants(gml + "registrationNumber").FirstOrDefault().Value;
                purch.purchname = goszakupka_root.Descendants(gml + "name").FirstOrDefault().Value;
                purch.purchurl = goszakupka_root.Descendants(gml + "urlOOS").FirstOrDefault().Value;
                purch.purchdescription = goszakupka_root.Descendants(gml1 + "legalAddress").FirstOrDefault().Value;
                purch.orgname = goszakupka_root.Descendants(gml1 + "fullName").FirstOrDefault().Value;
                purch.purchamount = goszakupka_root.Descendants(gml1 + "initialSum").FirstOrDefault().Value;
                var purchaseObjects = goszakupka_root.Descendants(gml1 + "lotItems").ToList();

                foreach (var item in purchaseObjects)
                {
                    purch.code = item.Descendants(gml1 + "code").FirstOrDefault().Value;
                    if (purch.code == null || purch.code == "")
                    {
                        purch.code = "0000000";
                    }
                    purch bufferpurch = new purch()
                    {
                        auctionbegindatestr = purch.auctionbegindatestr,
                        code = purch.code,
                        orgname = purch.orgname,
                        publicdatestr = purch.publicdatestr,
                        purchamount = purch.purchamount,
                        purchcode = purch.purchcode,
                        purchdescription = purch.purchdescription,
                        purchid = purch.purchid,
                        purchischanged = purch.purchischanged,
                        purchname = purch.purchname,
                        purchstatename = purch.purchstatename,
                        purchurl = purch.purchurl,
                        requestcount = purch.requestcount,
                        requestdatestr = purch.requestdatestr,
                        region = _region
                    };
                    if (buffer.Where(c => c.purchid == bufferpurch.purchid && c.code.Substring(0, 4) == bufferpurch.code.Substring(0, 4)).ToList().Count == 0)
                        buffer.Add(bufferpurch);
                }
                return buffer.ToList();
            }
            catch (Exception ex)
            {
                log.Error("ZakupkiGovFTP.XElementToPurch:{0}", ex.Message);
            }
            return null; ;
        }
        public override long GetLastDate(string filename)
        {
            Int64 rt = Int64.MaxValue;
            try
            {
                rt = Int64.Parse(filename.Substring(filename.Length - 33, 15).Replace('_', '0') + filename.Substring(filename.Length - 11, 3));
            }
            catch (Exception ex)
            {
                log.Error("ZakupkiGovFTP223.GetLastDate:{0}", ex.Message); 
            }
            return rt;
        }
    }
}

