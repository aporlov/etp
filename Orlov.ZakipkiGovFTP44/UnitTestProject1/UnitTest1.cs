﻿using System;
using ZakupkiGovFTP;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Xml.Linq;
using Analyzing;
using System.Linq;
using System.Collections.Generic;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestFTP()
        {
            string TestNameFile = "notification_Adygeja_Resp_2016050400_2016050500_001.xml.zip";
            SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder();
            conn.DataSource = @"adminbd\SQLEXPRESS";
            conn.InitialCatalog = "tradesystem";
            conn.IntegratedSecurity = true;
            ZakupkiGovFTP44 _zakupkigov = new ZakupkiGovFTP44(conn.ConnectionString);
            _zakupkigov.GetLastDate(TestNameFile);

        }
        [TestMethod]
        public void TestGetLastL()
        {
            string TestNameFile = "notification_Adygeja_Resp_2016050400_2016050500_001.xml.zip";
            SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder();
            conn.DataSource = @"adminbd\SQLEXPRESS";
            conn.InitialCatalog = "tradesystem";
            conn.IntegratedSecurity = true;
            ZakupkiGovFTP44 _zakupkigov = new ZakupkiGovFTP44(conn.ConnectionString);
            _zakupkigov.GetLastDate(TestNameFile);

        }

        [TestMethod]
        public void TestParsingXML()
        {
            SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder();
            conn.DataSource = @"adminbd\SQLEXPRESS";
            conn.InitialCatalog = "tradesystem";
            conn.IntegratedSecurity = true;
            ZakupkiGovFTP44 _zakupkigov = new ZakupkiGovFTP44(conn.ConnectionString);
            var test = XElement.Parse(File.ReadAllText("c:\\temp\\1.xml"), LoadOptions.PreserveWhitespace);
            List<purch> purxh = _zakupkigov.XElementToPurch(test,"region");

        }
         [TestMethod]
        public void TestProcessing()
        {
            SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder();
            conn.DataSource = @"adminbd\SQLEXPRESS";
            conn.InitialCatalog = "tradesystem";
            conn.IntegratedSecurity = true;
            ZakupkiGovFTP44 _zakupkigov = new ZakupkiGovFTP44(conn.ConnectionString);
            List<purch> _purch = _zakupkigov.StartTradesystem();
            var filter = _purch.Where(c => c.code.ToUpper().IndexOf("24.4") == 0).ToList();
        }
         [TestMethod]
         public void TestParsingXML223()
         {
             SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder();
             conn.DataSource = @"adminbd\SQLEXPRESS";
             conn.InitialCatalog = "tradesystem";
             conn.IntegratedSecurity = true;
             ZakupkiGovFTP223 _zakupkigov = new ZakupkiGovFTP223(conn.ConnectionString, "Cайт госзакупок 223ФZK");
             var test = XElement.Parse(File.ReadAllText("c:\\temp\\2.xml"), LoadOptions.PreserveWhitespace);
             List<purch> purxh = _zakupkigov.XElementToPurch(test, "region");

         }
         [TestMethod]
         public void TestParsingXML223ZK()
         {
             SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder();
             conn.DataSource = @"adminbd\SQLEXPRESS";
             conn.InitialCatalog = "tradesystem";
             conn.IntegratedSecurity = true;
             ZakupkiGovFTP223 _zakupkigov = new ZakupkiGovFTP223(conn.ConnectionString, "Cайт госзакупок 223ФЗZK");
             var test = XElement.Parse(File.ReadAllText("c:\\temp\\3.xml"), LoadOptions.PreserveWhitespace);
             List<purch> purxh = _zakupkigov.XElementToPurch(test, "region");

         }
         [TestMethod]
         public void TestGetLastDate()
         {
             SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder();
             conn.DataSource = @"adminbd\SQLEXPRESS";
             conn.InitialCatalog = "tradesystem";
             conn.IntegratedSecurity = true;
             ZakupkiGovFTP223 _zakupkigov = new ZakupkiGovFTP223(conn.ConnectionString, "Cайт госзакупок 223ФZK");
             Int64 i = _zakupkigov.GetLastDate("notification_Amurskaja_obl_2016123100_2017010100_002.xml.zip");
                                                                          
         }
       
    }
}
