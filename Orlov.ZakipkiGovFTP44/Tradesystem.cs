﻿using Analyzing;
using GZFtp;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ZakupkiGovFTP
{
    /// <summary>
    /// Торговая система
    /// </summary>
    public class _Tradesystem
    {
        protected int _TradeId;
        protected string _Tradename;
        protected string _Performedpurchid;
        protected string _TradeURL;
        protected Boolean _Working;
        protected string _IdURL;
        protected string _Lastpurchid;
        protected WorkingDatabase db;
        public static Logger log;
        public Func<string, bool> FilterForFilesonFTP;
        public _Tradesystem(string connectionstring, string NaimTradesystem)
        {
           Tradesystem _tradesystem;
           log = LogManager.GetLogger("ETPLog");
            try
            {
                db = new WorkingDatabase(connectionstring);
                 _tradesystem = db.GetTradeSystem(NaimTradesystem);
                 this._TradeId = _tradesystem.TradeId;
                 this._Tradename = _tradesystem.Tradename;
                 this._Performedpurchid = _tradesystem.PerformedPurchId;
                 this._TradeURL = _tradesystem.TradeURL;
                 this._Working = _tradesystem.Working;
                 this._IdURL = _tradesystem.IdURL;
                 this._Lastpurchid = _tradesystem.LastPurchId;
                 
                 
            }
            catch (Exception e)
            {
                log.Error("TradeSystem.constructor:{0}", e.Message);
            }
            
        }
        public Region GetRegion(string RegionName, int _TradeId)
       {
           try
           {
               return db.GetRegion(RegionName, _TradeId);

           }
           catch (Exception ex)
           {
               log.Error("TradeSystem.GetRegion:{0}",ex.Message);
               return null;
           }
       }
        public bool UpdateLastDateToRegion(string RegionName, string LastDate, int _TradeId)
       {
           try
           {
                db.UpdateLastDateToRegion(RegionName, LastDate, _TradeId);
                db.Save();
                return true;
            }
           catch (Exception ex)
           {
               log.Error("TradeSystem.UpdateLastDateToRegion:{0}", ex.Message);
               return false;
           }
       }
        public List<string> getregionlist(int FZ)
       {
           try
           {
               return db.regionlist(FZ);
           }
           catch (Exception ex)
           {
               log.Error("TradeSystem.regionlist:{0}", ex.Message);
               System.Console.ReadKey();
               return null;
           }
       }
        public List<purch> StartTradesystem()
        {
            List<XElement> xmlResult;
            List<string> bufferstring = null;
            List<string> regionlist = new List<string> {
            #region
            "Adygeja_Resp", 
            "Altaj_Resp", 
            "Altajskij_kraj",
            "Amurskaja_obl",
            "Arkhangelskaja_obl",
            "Astrakhanskaja_obl",
            "Bajkonur_g",
            "Bashkortostan_Resp",
            "Belgorodskaja_obl",
            "Brjanskaja_obl",
            "Burjatija_Resp",
            "Chechenskaja_Resp",
            "Cheljabinskaja_obl",
            "Chukotskij_AO",
            "Chuvashskaja_Resp",
            "Dagestan_Resp",
            "Evrejskaja_Aobl",
            "Ingushetija_Resp",
            "Irkutskaja_obl",
          //  "Irkutskaja_obl_Ust-Ordynskij_Burjatskij_okrug",
            "Ivanovskaja_obl",
            "Jamalo-Neneckij_AO",
            "Jaroslavskaja_obl",
            "Kabardino-Balkarskaja_Resp",
            "Kaliningradskaja_obl",
            "Kalmykija_Resp",
            "Kaluzhskaja_obl",
            "Kamchatskij_kraj",
            "Karachaevo-Cherkesskaja_Resp",
            "Karelija_Resp",
            "Kemerovskaja_obl",
            "Khabarovskij_kraj",
            "Khakasija_Resp",
           // "Khanty-Mansijskij_Avtonomnyj_okrug_-_Jugra_AO",
            "Kirovskaja_obl",
            "Komi_Resp",
            "Kostromskaja_obl",
            "Krasnodarskij_kraj",
            "Krasnojarskij_kraj",
            "Kurganskaja_obl",
            "Kurskaja_obl",
            "Leningradskaja_obl",
            "Lipeckaja_obl",
            "Magadanskaja_obl",
            "Marij_El_Resp",
            "Mordovija_Resp",
            "Moskovskaja_obl",
            "Moskva",
            "Murmanskaja_obl",
            "Neneckij_AO",
            "Nizhegorodskaja_obl",
            "Novgorodskaja_obl",
            "Novosibirskaja_obl",
            "Omskaja_obl",
            "Orenburgskaja_obl",
            "Orlovskaja_obl",
            "Penzenskaja_obl",
            "Permskij_kraj",
            "Primorskij_kraj",
            "Pskovskaja_obl",
            "Rjazanskaja_obl",
            "Rostovskaja_obl",
            "Sakha_Jakutija_Resp",
            "Sakhalinskaja_obl",
            "Samarskaja_obl",
            "Sankt-Peterburg",
            "Saratovskaja_obl",
           // "Severnaja_Osetija_-_Alanija_Resp",
            "Smolenskaja_obl",
            "Stavropolskij_kraj",
            "Sverdlovskaja_obl",
            "Tambovskaja_obl",
            "Tatarstan_Resp",
            "Tjumenskaja_obl",
            "Tomskaja_obl",
            "Tulskaja_obl",
            "Tverskaja_obl",
            "Tyva_Resp",
            "Udmurtskaja_Resp",
            "Uljanovskaja_obl",
            "Vladimirskaja_obl",
            "Volgogradskaja_obl",
            "Vologodskaja_obl",
            "Voronezhskaja_obl",
            "Zabajkalskij_kraj"
           // "Zabajkalskij_kraj_Aginskij_Burjatskij_okrug" 
            #endregion
             };
            List<purch> buffer = new List<purch>();
            log.Info("Tradesystem.StartTradesystem Начинаю качать :{0}", _Tradename);
            foreach (string region in regionlist)
            {
                Region regionfrombase = GetRegion(region, _TradeId);
                log.Trace("Tradesystem.StartTradesystem: Регион {0}", regionfrombase);
                long LastDate = Int64.Parse(regionfrombase.LastDate);
                log.Trace("Tradesystem.StartTradesystem: Последняя дата {0}", LastDate);
                FtpWork _ftpwork = new FtpWork(_TradeURL + regionfrombase.url);
                log.Trace("Tradesystem.StartTradesystem: URL {0}", _TradeURL + regionfrombase.url);
                bufferstring = _ftpwork.GetFileList().Where(FilterForFilesonFTP).Where(a=>GetLastDate(a)>LastDate).ToList();
                if (bufferstring.Count > 0)
                {
                    Int64 datefile = bufferstring.Select(n => GetLastDate(n)).Max();
                    string lastdate = Convert.ToString(datefile);
                    if (!this.UpdateLastDateToRegion(region, lastdate, _TradeId))
                    {
                        log.Error("ZakupkiGovFTP44.StartTradesystem: Не могу записать в базу UpdateLastToRegion");
                    }
                    else
                    {
                        log.Info("ZakupkiGovFTP44.StartTradesystem: зафиксировали регион:{0} Дата: {1}", region, lastdate);
                    }
                }
                foreach (string item in bufferstring)
                {
                    xmlResult = _ftpwork.DownloadFile(item);
                    foreach (var itemXml in xmlResult)
                    {
                        List<purch> bufferpurch = new List<purch>();
                        bufferpurch = XElementToPurch(itemXml, region);
                        if (bufferpurch != null) if (bufferpurch.Count > 0)
                                buffer.AddRange(bufferpurch.AsEnumerable());
                    }
                }
            }
            return buffer;
        }
        public virtual long GetLastDate(string filename)
        {
            throw new NotImplementedException();
        }
        public virtual List<purch> XElementToPurch(XElement itemXml, string region)
        {
            throw new NotImplementedException();
        }
    }
}
