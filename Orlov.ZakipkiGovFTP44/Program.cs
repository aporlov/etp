﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Analyzing;
namespace ZakupkiGovFTP
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SqlConnectionStringBuilder conn = new SqlConnectionStringBuilder();
                conn.DataSource = @".\SQLEXPRESS";
                conn.InitialCatalog = "tradesystem";
                conn.IntegratedSecurity = true;
                List<purch> _purch;
                ZakupkiGovFTP44 _zakupkigov = new ZakupkiGovFTP44(conn.ConnectionString);
                _purch = _zakupkigov.StartTradesystem();
                Createletter _createletter = new Createletter(conn.ConnectionString, "tradesystem@nnovgorod.siaint.ru", "Cайт госзакупок 44ФЗ");
                if (_purch != null) _createletter.Processing(_purch);
                //Электронные аукционы
                //ZakupkiGovFTP223 _zakupkigov223AE = new ZakupkiGovFTP223(conn.ConnectionString, "Cайт госзакупок 223ФЗAE");
                //_purch = _zakupkigov223AE.StartTradesystem();
                //if (_purch != null)
                //{
                //    _createletter.NameOfTradesystem= "Cайт госзакупок 223ФЗAE";
                //    _createletter.Processing(_purch);
                //}
                //Конкурс
                /*
                ZakupkiGovFTP223 _zakupkigov223ZK = new ZakupkiGovFTP223(conn.ConnectionString, "Cайт госзакупок 223ФЗZK");
                _purch = _zakupkigov223ZK.StartTradesystem();
                if (_purch != null)
                {
                    _createletter.NameOfTradesystem = "Cайт госзакупок 223ФЗZK";
                    _createletter.Processing(_purch);
                }
                */
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }
        }
    }
}
